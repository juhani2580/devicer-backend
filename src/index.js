import dotenv from "dotenv";
dotenv.config();
import path from "path";
import Koa from "koa";
import session from "koa-session";
import Router from "koa-router";
import bodyParser from "koa-bodyparser";
import cors from "koa-cors";
import knex from "knex";
import bcrypt from "bcrypt";
let db = knex({
  client: "sqlite3",
  connection: {
    filename: path.resolve(__dirname, "../dev.sqlite3")
  },
  useNullAsDefault: true,
  debug: true
});
let app = new Koa();
app.keys = [process.env.sessionSecretKey];
let sessionConfig = {};
let router = new Router();
app
  .use(cors())
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods())
  .use(session(sessionConfig, app))
  .use(ctx => {
    if (ctx.path === "/favicon.ico") {
      return;
    }
    let n = ctx.session.views || 0;
    ctx.session.views = ++n;
    ctx.body = n + " views";
  });
router.get("/foobar", ctx => {
  ctx.throw(500, "Bad request - Missing or invalid fields!");
});
router.get("/user", async ctx => {
  try {
    let res = await db.select().from("user");
    ctx.body = res;
  } catch (err) {
    ctx.throw(500, err);
  }
});
router.get("/device", async ctx => {
  try {
    let res = await db.select().from("device");
    ctx.body = res;
  } catch (err) {
    ctx.throw(500, err);
  }
});
router.get("/device_type", async ctx => {
  try {
    let res = await db.select().from("device_type");
    ctx.body = res;
  } catch (err) {
    ctx.throw(500, err);
  }
});
router.get("/software", async ctx => {
  try {
    let res = await db.select().from("software");
    ctx.body = res;
  } catch (err) {
    ctx.throw(500, err);
  }
});
router.get("/device_software", async ctx => {
  try {
    let res = await db.select().from("device_software");
    ctx.body = res;
  } catch (err) {
    ctx.throw(500, err);
  }
});
app.listen(process.env.appPort, () => {
  console.log(`Server now listening on port ${process.env.appPort}`);
});
