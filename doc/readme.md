# Documentation

## (In Finnish) 26.1. Sepon kommentit

* (ei prioriteetti) backup/revisio systeemi?
* (ei prioriteetti) lainaajat voivat kirjoittaa itse omat lainatiedot?
  - lainojen päivämäärät tietokantaan?
  - lainasta tiedot:
    - mitkä laitteet
    - kenelle
    - mihin projektiin
    - koska otti
    - koska palauttaa
  - karoliinan täytyy hyväksyä!
  - (ulkopuoliset eivät lainaa? ehkä vain lisätietokenttä riittää)
* device.status_info kentässä tieto esim. onko varastetusta laitteesta ilmoitettu poliisille
* lisenssien tilanne monimutkainen:
  - lisenssiavain ja/tai käyttäjänimi voi olla jaettuna usean koneen kesken
  - adobe potkii käyttäjän ulos jos jaettu liian monelle koneelle?
* käyttöliittymä: kenttien delete nappiin vahvistus: are you sure?
* yksi laite voi olla lainattuna usealle henkilölle samaan aikaan (esim. gamedev tyypit)
