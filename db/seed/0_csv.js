let path = require("path");
let csv2json = require("csvtojson");
exports.seed = function(knex, Promise) {
  return knex.transaction(async trx => {
    try {
      let tables = [
        "access_role",
        "team",
        "device",
        "device_type",
        "device_status",
        "user",
        "software",
        "owner"
      ];
      for (let table of tables) {
        let rows = await csv2json().fromFile(
          path.resolve(__dirname, "./data/", table + ".csv")
        );
        await trx(table).del();
        await knex.batchInsert(table, rows, 32).transacting(trx);
      }
      return trx.commit;
    } catch (e) {
      console.error(e);
      return trx.rollback;
    }
  });
};
