exports.up = function(knex, Promise) {
  return knex.schema
    .createTable("software", table => {
      table.increments();
      table.text("value").notNullable();
      table
        .boolean("is_operating_system")
        .notNullable()
        .defaultTo(false);
    })
    .createTable("device_software", table => {
      table.increments();
      table
        .integer("device_id")
        .notNullable()
        .references("id")
        .inTable("device");
      table
        .integer("software_id")
        .notNullable()
        .references("id")
        .inTable("software");
      table.text("license_key");
      table.text("additional_info");
    })
    .createTable("device_type", table => {
      table.increments();
      table.text("value").notNullable();
    })
    .createTable("device_status", table => {
      table.increments();
      table.text("value").notNullable();
    })
    .createTable("owner", table => {
      table.increments();
      table.text("value").notNullable();
    })
    .createTable("device", table => {
      table.increments();
      table.timestamps();
      table
        .integer("device_type_id")
        .notNullable()
        .references("id")
        .inTable("device_type");
      table
        .integer("owner_id")
        .notNullable()
        .references("id")
        .inTable("owner");
      table
        .integer("device_status_id")
        .notNullable()
        .references("id")
        .inTable("device_status");
      table.text("additional_device_status_info");
      table.text("label").notNullable();
      table.text("manufacturer_and_model");
      table
        .integer("quantity")
        .notNullable()
        .defaultTo(1);
      table.text("serial_number");
      table.text("ip_address");
      table.text("cpu");
      table.text("ram");
      table.text("hdd");
      table.text("gpu");
      table.text("purchase_store");
      table.dateTime("purchase_date");
      table.text("warranty_info");
      table.text("additional_info");
    })
    .createTable("reservation", table => {
      table.increments();
      table.timestamps();
      table
        .integer("device_id")
        .notNullable()
        .references("id")
        .inTable("device");
      table.boolean("is_permanent_loan").notNullable();
      table.dateTime("start_date");
      table.dateTime("end_date");
      table.boolean("was_returned").defaultTo(false);
      table.text("project");
      table.text("additional_info");
    })
    .createTable("user_reservation", table => {
      table.increments();
      table
        .integer("user_id")
        .notNullable()
        .references("id")
        .inTable("user");
      table
        .integer("reservation_id")
        .notNullable()
        .references("id")
        .inTable("reservation");
    })
    .createTable("user", table => {
      table.increments();
      table.timestamps();
      table
        .integer("access_role_id")
        .notNullable()
        .references("id")
        .inTable("access_role");
      table
        .integer("team_id")
        .references("id")
        .inTable("team");
      table.text("first_name");
      table.text("last_name");
      table.text("username");
      table.text("password_hash");
      table.text("title");
      table.text("phone");
      table.text("email");
      table.text("location");
      table.datetime("start_date");
      table.datetime("end_date");
      table.text("active_directory_account");
      table.text("gsuite_account");
      table.text("slack_account");
      table.text("other_account");
      table.boolean("has_keys").defaultTo(false);
    })
    .createTable("team", table => {
      table.increments();
      table.text("value").notNullable();
    })
    .createTable("access_role", table => {
      table.increments();
      table.text("value").notNullable();
    });
};
exports.down = function(knex, Promise) {
  return knex.schema
    .dropTableIfExists("device_software")
    .dropTableIfExists("software")
    .dropTableIfExists("user_reservation")
    .dropTableIfExists("reservation")
    .dropTableIfExists("device")
    .dropTableIfExists("device_status")
    .dropTableIfExists("owner")
    .dropTableIfExists("device_type")
    .dropTableIfExists("user")
    .dropTableIfExists("team")
    .dropTableIfExists("access_role");
};
